﻿using System;
using System.Threading;
using RabbitMQ.Client;

namespace MessageBrokers.Models.Consumers
{
    public abstract class RabbitMQListener : WorkListener<string>
    {
        public string QueueName { get; private set; }
        public string Topic { get; private set; }

        private ConnectionFactory _factory;

        protected RabbitMQListener(string host, string username, string password, string topic)
            : base(host, username, password)
        {
            Topic = topic;

            _factory = new ConnectionFactory()
            {
                HostName = HostName,
                UserName = UserName,
                Password = Password,
            };

            using (var connection = _factory.CreateConnection())
            {
                using (var channel = connection.CreateModel())
                {
                    channel.ExchangeDeclare(exchange: Topic, type: "fanout");

                    QueueName = channel.QueueDeclare().QueueName;

                    channel.QueueBind(queue: QueueName,
                                      exchange: Topic,
                                      routingKey: "");
                }
            }
        }

        public override string ListenForWork()
        {
            var work = String.Empty;
            using (var connection = _factory.CreateConnection())
            {
                using (var channel = connection.CreateModel())
                {
                    while (String.IsNullOrWhiteSpace(work))
                    {
                        work = GetMessage(channel);
                        Thread.Sleep(1);
                    }

                    return work;
                }
            }
        }

        protected abstract string GetMessage(IModel channel);

    }
}
